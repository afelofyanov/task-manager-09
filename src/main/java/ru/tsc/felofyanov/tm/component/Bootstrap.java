package ru.tsc.felofyanov.tm.component;

import ru.tsc.felofyanov.tm.api.ICommandController;
import ru.tsc.felofyanov.tm.api.ICommandRepository;
import ru.tsc.felofyanov.tm.api.ICommandService;
import ru.tsc.felofyanov.tm.constant.ArgumentConst;
import ru.tsc.felofyanov.tm.constant.TerminalConst;
import ru.tsc.felofyanov.tm.controller.CommandController;
import ru.tsc.felofyanov.tm.repository.CommandRepository;
import ru.tsc.felofyanov.tm.service.CommandService;

import java.util.Scanner;

public final class Bootstrap {

    private static final ICommandRepository commandRepository = new CommandRepository();

    private static final ICommandService commandService = new CommandService(commandRepository);

    private static final ICommandController commandController = new CommandController(commandService);

    public static void close() {
        System.exit(0);
    }

    public void run(String[] args) {
        if (processArgument(args)) System.exit(0);
        commandController.showWelcome();
        final Scanner scanner = new Scanner(System.in);
        while (true) {
            System.out.println("ENTER COMMAND");
            final String command = scanner.nextLine();
            processCommand(command);
        }
    }

    public void processCommand(final String command) {
        if (command == null || command.isEmpty()) return;
        switch (command) {
            case TerminalConst.ABOUT:
                commandController.showAbout();
                break;
            case TerminalConst.VERSION:
                commandController.showVersion();
                break;
            case TerminalConst.HELP:
                commandController.showHelp();
                break;
            case TerminalConst.EXIT:
                close();
                break;
            case TerminalConst.INFO:
                commandController.showSystemInfo();
                break;
            case TerminalConst.COMMANDS:
                commandController.showCommands();
                break;
            case TerminalConst.ARGUMENTS:
                commandController.showArguments();
                break;
            default:
                commandController.showErrorCommand(command);
                break;
        }
    }

    public void processArgument(final String arg) {
        if (arg == null || arg.isEmpty()) return;
        switch (arg) {
            case ArgumentConst.ABOUT:
                commandController.showAbout();
                break;
            case ArgumentConst.VERSION:
                commandController.showVersion();
                break;
            case ArgumentConst.HELP:
                commandController.showHelp();
                break;
            case ArgumentConst.INFO:
                commandController.showSystemInfo();
                break;
            case ArgumentConst.COMMANDS:
                commandController.showCommands();
                break;
            case ArgumentConst.ARGUMENTS:
                commandController.showArguments();
                break;
            default:
                commandController.showErrorArgument(arg);
                break;
        }
    }

    public boolean processArgument(String[] args) {
        if (args == null || args.length == 0) return false;
        final String arg = args[0];
        processArgument(arg);
        return true;
    }
}
