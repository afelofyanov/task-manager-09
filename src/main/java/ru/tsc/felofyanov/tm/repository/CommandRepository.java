package ru.tsc.felofyanov.tm.repository;

import ru.tsc.felofyanov.tm.api.ICommandRepository;
import ru.tsc.felofyanov.tm.constant.ArgumentConst;
import ru.tsc.felofyanov.tm.constant.TerminalConst;
import ru.tsc.felofyanov.tm.model.Command;

public class CommandRepository implements ICommandRepository {

    private static final Command INFO = new Command(
            TerminalConst.INFO, ArgumentConst.INFO,
            "Show system info."
    );

    private static final Command ABOUT = new Command(
            TerminalConst.ABOUT, ArgumentConst.ABOUT,
            "Show developer info."
    );

    private static final Command VERSION = new Command(
            TerminalConst.VERSION, ArgumentConst.VERSION,
            "Show applicant version."
    );

    private static final Command HELP = new Command(
            TerminalConst.HELP, ArgumentConst.HELP,
            "Show terminal commands info."
    );

    private static final Command EXIT = new Command(
            TerminalConst.EXIT, null,
            "Close application."
    );

    private static final Command COMMANDS = new Command(
            TerminalConst.COMMANDS, ArgumentConst.COMMANDS,
            "Show command list."
    );

    private static final Command ARGUMENTS = new Command(
            TerminalConst.ARGUMENTS, ArgumentConst.ARGUMENTS,
            "Show argument list."
    );

    private final Command[] terminalCommands = new Command[]{
            INFO,
            ABOUT,
            VERSION,
            HELP,
            EXIT,
            ARGUMENTS,
            COMMANDS
    };

    @Override
    public Command[] getCommands() {
        return terminalCommands;
    }
}
